package com.muqeem.myapplication

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.findNavController
import com.muqeem.myapplication.databinding.FragmentFirstBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
const val CAR_PARKING_CHARGE: Double= 100.0
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvParkingCharge.text = binding.tvParkingCharge.text.toString().replace("%1$", ""+ CAR_PARKING_CHARGE)
        binding.evAmount.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                binding.tvBalance.text = ""
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })

        binding.buttonFirst.setOnClickListener {
            validateInputAmount(binding.evAmount.text.toString())

        }
    }

    private fun validateInputAmount(amount: String){
        if(TextUtils.isEmpty(amount)){
            Toast.makeText(activity, getString(R.string.please_enter_amount), Toast.LENGTH_LONG)
                .show()
            return
        }
        if (amount != null) {
            val balance: Double = doTransaction(CAR_PARKING_CHARGE, amount.toDouble())
            if(balance >= 0)
            binding.tvBalance.text = getString(R.string.balance_left) +  " " + balance + " R"
        }
    }

    private fun doTransaction(parkingCharge: Double, inputAmount: Double): Double{
        if(parkingCharge > inputAmount){
            Toast.makeText(activity, getString(R.string.transaction_failure_message), Toast.LENGTH_LONG)
                .show()
            return 0.0
        }

        Toast.makeText(activity, getString(R.string.transaction_success_message), Toast.LENGTH_LONG).show()

        return inputAmount - parkingCharge

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}